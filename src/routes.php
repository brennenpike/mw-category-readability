<?php

use DaveChild\TextStatistics as TS;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Home page with basic template and category input form.
 */
$app->get( '/', function (Request $request, Response $response, array $args) {
	// Render index view
	return $this->renderer->render( $response, 'index.phtml', $args );
} );

/**
 * An endpoint for getting category data by handing a request off to a
 * MediaWiki server.
 */
$app->get( '/category', function (Request $request, Response $response, array $args) {
	$category = $request->getQueryParam('cat');
	$type = $request->getQueryParam('type');
	
	$queryParams = [
		'action' => 'query',
		'format' => 'json',
		'generator' => 'categorymembers',
		'gcmtitle' => "Category:$category",
		'gcmlimit' => '60',
		'prop' => 'extracts',
		'exintro' => '1',
		'explaintext' => '1',
	];

	$textStatistics = new TS\TextStatistics;
	$pageList = [];
	$error = null;
	$endpoint = $this->get( 'settings' )['mwEndpoint'];

	while ( true ) {
		// Since we can only retrieve 20 extracts at a time, we'll need to continue
		// through the full list:
		if ( isset( $excontinue ) ) {
			$queryParams['excontinue'] = $excontinue;
		}
		
		list( $categoryData, $apiStatus ) = apiRequest( $endpoint, $queryParams );

		if ( $apiStatus !== 200 ) {
			$error = 'MediaWiki API request failed.';
			break;
		}

		if ( !isset( $categoryData['query']['pages'] ) ) {
			$error = 'No pages found for category.';
			break;
		}

		// Assign readability scores to each extract and build a list:
		foreach ( $categoryData['query']['pages'] as $page ) {

			// Skip if no intro or no extract for page in this part of the resultset:
			if ( !isset( $page['extract'] ) || !strlen( $page['extract'] ) ) {
				continue;
			}

			$score = $textStatistics->daleChallReadabilityScore( $page['extract'] );
			$pageList[] = [ $page['title'], $page['extract'], $score ];

		}

		$excontinue = null;
		if ( isset( $categoryData['continue']['excontinue']) ) {
			$excontinue = $categoryData['continue']['excontinue'];
		} else {
			// No more extract results.
			break;
		}
	}

	// Initial sort by readability score:
	usort( $pageList, function ($pageA, $pageB) {
		return $pageA[2] <=> $pageB[2];
	} );

	// Either send back raw JSON or return the home page with data
	// the template can use to build a table:

	if ( $type === 'json' ) {
		return $response->withJson( [
			'pageList' => $pageList,
			'error' => $error
		] );
	} else {
		$templateVars = [
			'pageList' => $pageList,
			'error' => $error,
			'cat' => $category
		];
		return $this->renderer->render(
			$response,
			'index.phtml',
			$templateVars
		);
	}
} );

/**
 * Take a MediaWiki API endpoint and some query parameters, use cURL to
 * retrieve results.
 *
 * @param string $endpoint URL of API endpoint
 * @param array $queryParams collection of GET parameters
 * @return array containing json_decode()ed API response and HTTP status code
 */
function apiRequest ( $endpoint, $queryParams ) {
	$queryString = http_build_query( $queryParams );
	
	$ch = curl_init( $endpoint . $queryString );
	
	curl_setopt( $ch, \CURLOPT_RETURNTRANSFER, 1 );
	curl_setopt( $ch, CURLOPT_USERAGENT, "MediaWikiCategoryReadability/0.0.1 (mediawiki@chaff.p1k3.com)" );
	
	$apiResponse = curl_exec( $ch );
	$apiStatus = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
	
	curl_close( $ch );
	
	$categoryData = json_decode( $apiResponse, true );
	
	return [ $categoryData, $apiStatus ];
}
